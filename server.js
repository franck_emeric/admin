
const express = require('express');

const app = express();

//app.get('*', function(req, res) {  
  //  res.redirect('https://' + req.headers.host + req.url);
//})

const cors=require('cors')

app.use(cors())

app.use(express.static('./dist/Admin'));


app.get('/*',(req,res)=>{
    res.sendFile('index.html',{root:'./dist/Admin'});
});


app.listen(process.env.PORT || 8080)