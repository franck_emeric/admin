import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductionComponent } from './production/production.component';
import { ProductionsRoutingModule } from './productions-routing.module';
import { PdfcreatorComponent } from './pdfcreator/pdfcreator.component';
import { PdfcreaComponent } from './pdfcrea/pdfcrea.component';


@NgModule({
  declarations: [
    ProductionComponent,
    PdfcreatorComponent,
    PdfcreaComponent 
    ],
  imports: [
    CommonModule,
    ProductionsRoutingModule
  ]
})
export class ProductionsModule { }
