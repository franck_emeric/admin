import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConseilclientsRoutingModule } from './conseilclients-routing.module';
import { ConseillerComponent } from './conseiller/conseiller.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ConseillerComponent
  ],
  imports: [
    CommonModule,
    ConseilclientsRoutingModule,
    FormsModule
  ]
})
export class ConseilclientsModule { }
