import { Component, OnInit ,OnChanges} from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
declare var require: any
var myalert=require('sweetalert2')
@Component({
  selector: 'app-conseiller',
  templateUrl: './conseiller.component.html',
  styleUrls: ['./conseiller.component.scss']
})
export class ConseillerComponent implements OnInit, OnChanges {

  

  constructor(private monhttp:HttpService) { }
  orders:any=[]
  filtreorder:any=[]
  
  pendingorders:any=[]
  okorders:any=[]
  cancelorders:any=[]
  tab:any=[];
  cpt=0
  crea_print_items:any=[]
  crea_okprint_items:any=[]
  crea_acpprint_items:any=[]
  crea_cancelprint_items:any=[]

  crea_pack_items:any=[]
  crea_okpack_items:any=[]
  crea_acppack_items:any=[]
  crea_cancelpack_items:any=[]

  crea_gadget_items:any=[]
  crea_okgadget_items:any=[]
  crea_acpgadget_items:any=[]
  crea_cancelgadget_items:any=[]

  crea_cloth_items:any=[]
  crea_cancelcloth_items:any=[]
  crea_okcloth_items:any=[]
  crea_acpcloth_items:any=[]

  crea_disp_items:any=[]
  crea_okdisp_items:any=[]
  crea_acpdisp_items:any=[]
  crea_canceldisp_items:any=[]

  editor_items:any=[]
  acpeditor_items:any=[]
  okeditor_items:any=[]
  canceleditor_items:any=[]
  date:any;
  dat:any
 today:any;
 d:any
 hours:any
 fullDate:any
 options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
 public dte:any
 ngOnChanges(){

  this.dat=new Date();
  this.dat= this.dat.toLocaleString("fr",this.options);
 }
 ngOnInit(): void {
     this.d = new Date();
    this.date = this.d.getFullYear()+'-'+(this.d.getMonth()+1)+'-'+this.d.getDate();
   this.hours = this.d.getHours() + ":" + this.d.getMinutes() + ":" + this.d.getSeconds();
    this.fullDate= this.date+'-'+this.hours;

   this.ngOnChanges();

    this.monhttp.Aladin_orders().subscribe(
      res=>{
        this.orders=res.data
      
       console.log(this.orders)
        for(let item of this.orders){
          item.description= JSON.parse(item.description)
         
          if(item.description.type_product=="crea" && item.description.category=="vetement"){
            
              this.crea_cloth_items.push(item)
           
           }
          if(item.description.type_product=="crea" && item.description.category=="emballage"){ 
            this.crea_pack_items.push(item)
            console.log(this.crea_pack_items)
           
          }
          if(item.description.type_product=="editor" ){
            this.editor_items.push(item)
          }
      if(item.description.type_product=="crea" && item.description.category=="imprimer"){
        this.crea_print_items.push(item)
      }
      if(item.description.type_product=="crea" && item.description.category=="affichage"){
        this.crea_disp_items.push(item)
        
      }
      if(item.description.type_product=="crea" && item.description.category=="gadget"){
        this.crea_gadget_items.push(item)
      
      }
     
    }
    
  
},
      err=>{
        console.log(err)
      }

    )
    this.monhttp.Getpending().subscribe(
      res=>{
        this.pendingorders=res.data
        for(let items of this.pendingorders){
          
          items.description= JSON.parse(items.description)
          if(items.description.type_product=="crea" && items.description.category=="vetement"){
            
            
              this.crea_acpcloth_items.push(items)
              
            
          }
          if(items.description.type_product=="crea" && items.description.category=="emballage"){
           this.crea_acppack_items.push(items)
           
          }
          if(items.description.type_product=="crea" && items.description.category=="imprimer"){
           this.crea_acpprint_items.push(items)
           
          }
          if(items.description.type_product=="crea" && items.description.category=="affichage"){
           this.crea_acpdisp_items.push(items)
           
          }
          if(items.description.type_product=="crea" && items.description.category=="gadget"){
           this.crea_acpgadget_items.push(items)
           
          }
         
         if(items.description.type_product=="editor" ){
           this.acpeditor_items.push(items)
         }
        
         console.log(this.crea_acpcloth_items)
          }
      },
      err=>{
        console.log(err)
      }

    )
  this.monhttp.GetordersLivre().subscribe(
    res=>{
      this.okorders=res.data
      console.log(this.okorders)
      for(let items of this.okorders){
        items.description= JSON.parse(items.description)
       if(items.description.type_product=="crea" && items.description.category=="vetement"){
       
      

          this.crea_okcloth_items.push(items)
       
        
       }
       if(items.description.type_product=="crea" && items.description.category=="emballage"){
        this.crea_okpack_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="imprimer"){
        this.crea_okprint_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="affichage"){
        this.crea_okdisp_items.push(items)
        
       }
       if(items.description.type_product=="crea" && items.description.category=="gadget"){
        this.crea_okgadget_items.push(items)
        
       }
      if(items.description.type_product=="editor" ){
        this.okeditor_items.push(items)
      }

    }
      
    }
    ,
      err=>{
        console.log(err)
      }
  )
  this.monhttp.getCancel().subscribe(
    res=>{
     this.cancelorders=res.data
     console.log(this.cancelorders)
    for(let items of this.cancelorders){
      items.description= JSON.parse(items.description)
     if(items.description.type_product=="crea" && items.description.category=="vetement"){
     
      
        this.crea_cancelcloth_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="emballage"){
      this.crea_cancelpack_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="imprimer"){
      this.crea_cancelprint_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="affichage"){
      this.crea_canceldisp_items.push(items)
      
     }
     if(items.description.type_product=="crea" && items.description.category=="gadget"){
      this.crea_cancelgadget_items.push(items)
      
     }
    if(items.description.type_product=="editor" ){
      this.canceleditor_items.push(items)
    }
  }
   console.log(this.crea_cancelcloth_items)
    },
    err=>{
      console.log(err)
    }
  )
  }

updateStatus(event:any){
 let id = event.target.id
 if(id!=undefined){
  
   let data={id:id,status:'pending'}
   this.monhttp.Updatestatus(data).subscribe(
     res=>{
       console.log(res)
       if(res.status==true){
         this.monhttp.Getpending().subscribe(
           res=>{
            
             this.pendingorders=res.data
             console.log( this.pendingorders)
             myalert.fire({
              icon: "success",
               button: "Ok"
              });
              location.reload()
           }
         )
       }
     },
     err=>{
       console.log(err)
     }
   )
 }
 
}
updateok(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'ok'}
    this.monhttp.updateok2(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.GetordersLivre().subscribe(
            res=>{
              
              this.okorders=res.data
              console.log(this.okorders)
              myalert.fire({
                icon: "success",
                 button: "Ok"
                });
                location.reload()
            }
           
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}
updatecancel(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'rejected'}
    this.monhttp.Updatecancel(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.getCancel().subscribe(
            res=>{
              
              this.cancelorders=res.data
              console.log(this.cancelorders)
              myalert.fire({
                text:"commande annulée",
                icon: "success",

                 button: "Ok"
                });
              location.reload()
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}



logout(event:any){
  localStorage.removeItem('user');
  localStorage.removeItem('role');
  location.reload();
}
orderInfo(event:any){
       
     
       
  }
    
  
    
  
    
  

}
