import { TestBed } from '@angular/core/testing';

import { CCGuard } from './cc.guard';

describe('CCGuard', () => {
  let guard: CCGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CCGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
