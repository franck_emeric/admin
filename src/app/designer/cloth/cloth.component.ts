import { Component, OnInit ,OnChanges} from '@angular/core';
import { ColorEvent } from 'ngx-color';
import { HttpService } from 'src/app/services/http.service';
declare var require:any;
var myalert=require('sweetalert2')
@Component({
  selector: 'app-cloth',
  templateUrl: './cloth.component.html',
  styleUrls: ['./cloth.component.scss']
})
export class ClothComponent implements OnInit ,OnChanges{
  colors=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];
  current_color:any;
  current_array:any=["white"];
  file1:any;
  file2:any;
  file3:any;
  file4:any;
  file:any;
  image:any;
  Images:any=[];
  img_id:any
  staff:any;
  insertId:any;
  imageset=false;
  f:any;
  progres=0;
  step_one={
    name:"",
    comment:"",
    price:0,
    file1:"",
    file2:"",
    file3:"",
    file4:""
  }
  loading=false;
  step_two={
    m:"",
    l:"",
    xl:"",
    xxl:""
  }

  step_three={
    sg:"",
    fl:""
   
  }
  is_step_two=false;
  is_step_one=true;
  is_step_three=false;
  is_step_four=false;
  is_step_five=false;


  constructor(private service:HttpService) { }
  ngOnChanges(){
  this.updateProgress();

}

toggle(){
  this.loading=!this.loading;
}
  ngOnInit(): void {
    this.current_color=this.colors[5];
    let color;
  
  this.staff=localStorage.getItem('user');

  this.service.getcolor().subscribe(
    res=>{
     color=res;
     for(let item of color){
       if(this.colors.indexOf(item)==-1){
        this.colors.push(item.lib);

       }
     }
     
    }
  )

  }
  

  changeComplete($event:ColorEvent){
    console.log($event.color.hex);
    this.current_array.push($event.color.hex);
    this.current_color=$event.color.hex;
    var elt=document.getElementById('file_color');
    if($event.color.hex){
      this.service.triggerMouse(elt);

    }

  }

  file_upload(event:any){
    this.file=event.target.files[0];
    if(!this.service.handleChange(this.file)){
        console.log(!this.service.handleChange(this.file));
    let id= event.target.id;
    if(id=="file1_cloth"){
    
      this.step_one.file1=this.file;
      //Object.assign(this.step_one,{file1:this.file})
      const reader = new FileReader();
      reader.onload = () => {
       this.image = reader.result;
       this.file1=this.image;
       this.imageset=true
       this.Images.push(this.image);
       };
       reader.readAsDataURL(this.file);
    }


    if(id=="file2_cloth"){
     // Object.assign(this.step_one,{file2:this.file})
     this.step_one.file2=this.file;
      const reader = new FileReader();
      reader.onload = () => {
       this.image = reader.result;
       this.file2=this.image;
       this.imageset=true
       this.Images.push(this.image);
       };
       reader.readAsDataURL(this.file);
    }

    if(id=="file3_cloth"){
      //Object.assign(this.step_one,{file3:this.file})
      this.step_one.file3=this.file;

      const reader = new FileReader();
      reader.onload = () => {
       this.image = reader.result;
       this.file3=this.image;
       this.imageset=true
       this.Images.push(this.image);
       };
       reader.readAsDataURL(this.file);
    }

    if(id=="file4_cloth"){
      //Object.assign(this.step_one,{file4:this.file})
      this.step_one.file4=this.file;
      const reader = new FileReader();
      reader.onload = () => {
       this.image = reader.result;
       this.file4=this.image;
       this.imageset=true
       this.Images.push(this.image);
       };
       reader.readAsDataURL(this.file);
    }


  }
  }


  file_Upload(event:any){
    this.file=event.target.files[0];
    if(!this.service.handleChange(this.file)){
      const reader = new FileReader();
    reader.onload = () => {
     this.image = reader.result;
     this.imageset=true;
    // this.file1=this.image;
     this.Images.push(this.image);
     };
     reader.readAsDataURL(this.file);
    }
    

  
  }
  
  _Upload(event:any){
    this.f=event.target.files[0];
    if(!this.service.handleChange(this.f)){
      const reader = new FileReader();
    reader.onload = () => {
     this.image = reader.result;
     this.imageset=true;
     this.Images.push(this.image);
     };
     reader.readAsDataURL(this.f);
    }
    

    
  }


  onUploaded=()=>{
    let data={
      img:this.file,insertedId:this.insertId,lib:this.current_color,link:this.f
    }
   this.saveClothColor(data);
    
  }


  updateProgress(){
    if(this.step_one.name !=""&& this.step_one.comment!="" && this.step_one.price!=0){
      console.log(this.step_one);
      this.progres =this.progres+20;
      document.getElementById('prog')?.setAttribute('style',"width:"+this.progres+"%");
      return true
    }else{
      return false
    }

    }

    getback(){
      if(this.step_one.name !=""&& this.step_one.comment!="" && this.step_one.price!=0){
        console.log(this.step_one);
        this.progres =this.progres-20;
        document.getElementById('prog')?.setAttribute('style',"width:"+this.progres+"%");
        return true
      }else{
        return false
      }
    }
  
    get_back_to_step_one(){
      if(this.getback()){
        this.is_step_one=this.is_step_two;
        this.is_step_two=!this.is_step_one;
      }
   
    }

    get_back_to_step_two(){
      if(this.getback()){
        this.is_step_two=this.is_step_three;
        this.is_step_three=!this.is_step_two;
       }
   
    }

    get_back_to_step_three(){
     
    // this.is_step_three=false;
    console.log("ok")

    }

    get_back_to_step_four(){
      this.is_step_five=!this.is_step_five;
      this.is_step_four=!this.is_step_four
    
    }
   
  get_step_two(){
    if(this.updateProgress()){
      console.log(this.step_one);
      this.is_step_two=!this.is_step_two;
      this.is_step_one=!this.is_step_one;
      //this.updateProgress();
    }
    
  }


   
  get_step_three(){
    if(this.updateProgress()
    ){
      console.log(this.step_two);
      this.is_step_two=!this.is_step_two;
      this.is_step_three=!this.is_step_three;
    }
    
  }
getone(){
  if(this.step_one.name !=""&& this.step_one.comment!="" && this.step_one.price!=0){
    console.log(this.step_one);
    this.progres =0;
    document.getElementById('prog')?.setAttribute('style',"width:"+this.progres+"%");
    return true
  }else{
    return false
  }
}

   
  get_step_four(){
    if(this.updateProgress()){
      console.log(this.step_three);
      this.savecloth();
    }
  
    //this.updateProgress();
  }

get_to_step_one(){
 if(this.getone()){
  this.is_step_five=!this.is_step_five;
  this.is_step_one=!this.is_step_one;
  this.Images=[];

 }
 
}

  get_step_five(){
    if(this.updateProgress()){
      this.is_step_five=!this.is_step_five;
      this.is_step_four=!this.is_step_four;
      this.progres=this.progres+15;
      document.getElementById('prog')?.setAttribute('style',"width:"+this.progres+"%");

     // this.updateProgress();
    }
  
  }

  savecloth(){
    let data={};
    Object.assign(data,{img:this.step_one?.file1,name:this.step_one?.name,price:this.step_one?.price,description:this.step_one?.comment,size:JSON.stringify(this.step_two),type:JSON.stringify(this.step_three),is_ordered:1,staff:(+this.staff)});
  if(data){
    this.toggle();
    this.service.Insertclothdata(data).subscribe(
      res=>{
        if(res.data!=undefined){
         // this.toggle();
          this.insertId=res.data.insertId
          this.InsertclothImages()
          console.log(res);
          
        }
        else{
          this.toggle();
          console.log(res)
          myalert.fire({
          title: "Erreur",
          text: res.massage,
          icon: "error",
           button: "Ok"
          })
  
        }
      },
  err=>{
    console.log(err)
    this.toggle();
    myalert.fire({
      title: "Erreur",
      text: "une erreur s'est produite l'insertion à échoué!!",
      icon: "error",
       button: "Ok"
      })
  }
    )
  }
  

  }


  InsertclothImages(){
    let clothImgdata={}
    Object.assign(clothImgdata,{insertId:this.insertId,file1:this.step_one.file1,file2:this.step_one.file2,file3:this.step_one.file3,file4:this.step_one.file4});

    this.service.InsertClothImage(clothImgdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.img_id=res.data.insertId
          this.toggle()
          myalert.fire({
            title: "Enregistrement",
            text: res.message,
            icon: "success",
             button: "Ok"
            });
            this.is_step_three=!this.is_step_three;
            this.is_step_four=!this.is_step_four;
  
        }else{
          this.toggle()
          myalert.fire({
          title: "Erreur",
          text: res.message,
          icon: "error",
           button: "Ok"
          })

        }
      },
      err=>{
        this.toggle();
        console.log(err);
        myalert.fire({
          title: "Erreur",
          text: "une erreur s'est produite l'insertion à échoué!!",
          icon: "error",
           button: "Ok"
          });
      }
    )
  }

saveClothColor(data:any){
  this.toggle()
  this.service.InsertClothcolor(data).subscribe(res=>{
    this.toggle()
    if(res.status==true){
      myalert.fire({
        title: "Enregistrement",
        text: res.message,
        icon: "success",
         button: "Ok"
        });
        var el = document.getElementById("saveclr");
        this.service.triggerMouse(el);
    }else{
      myalert.fire({
        title: "Erreur",
        text:res.message,
        icon: "error",
         button: "Ok"
        })
    }
    console.log(res);
  },
  err=>{
    this.toggle()
    myalert.fire({
      title: "Erreur",
      text: "une erreur s'est produite",
      icon: "error",
       button: "Ok"
      });
    console.log(err);
  }
  
  );
}



}
