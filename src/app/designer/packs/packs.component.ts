import { sha1 } from '@angular/compiler/src/i18n/digest';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit } from '@angular/core';
import { ColorEvent } from 'ngx-color';
import { HttpService } from 'src/app/services/http.service';
declare var require:any;
var myalert=require('sweetalert2')

@Component({
  selector: 'app-packs',
  templateUrl: './packs.component.html',
  styleUrls: ['./packs.component.scss']
})
export class PacksComponent implements OnInit {
 list_sac=["15/20","20/25","25/30","28/35","35/45","45/50","50/55"];
 list_sachet=["20/30","30/30","33/40","40/45","50/60"];
 list_color=[1,2,3,4]

 prix_sac=[5000,5000,7000,7000,10000,15000,15000];
 prix_sachet=[3000,5000,7000,15000,15000];
 color=5000;
 @Input() sac=true;
@Input() sachet=false;
sac_name:any;
sac_dec:any;
sac_size:any;
sac_price:any;
sachet_size:any;
sachet_price:any;
sachet_dec:any;
sachet_name:any;
sachet_has_color=false;
 first=true;
 second=false;
 third=false;
 fourth=false;
 enteredcolor:any
 hascolor=false;
 url1:any;
 url2:any;
 url3:any
 url4:any;
 url5:any;
 url6:any;
sh=false
cpt=0;
staff:any;
insertId:any;

 colors=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];

  constructor(private http:HttpService ) { }

  ngOnInit(): void {
    this.staff = localStorage.getItem('user');

  }


  gotoSecond(){
    this.first=!this.first;
    this.second=!this.second;
    if(!this.sac){
      this.sachet=!this.sachet;
      this.sh=!this.sh
    }
   
  
    console.log(this.sac_dec,this.sac_name,this.sac_size)
  }

  gotoThirdSh(){
    this.second=!this.second;
    this.third=!this.third;
    
  }

  gotoSecondsacht(){
    this.sachet=!this.sachet;
    this.second=!this.second;

  }

  gotoThird(){
    this.second=!this.second;
    this.third=!this.third;

   this.savepack();

  }

  OnsizeChange(event:any){

    if(event.target.value!=""){
      this.sac_size=event.target.value

    }

  }

savepack(){
  let data={
    staff:this.staff,
    is_ordered:1,
    url1:this.url3,
    comment:this.sac_dec,
    name:this.sac_name,
    dim:this.sac_size,
  }
  if(this.sac_size==this.list_sac[0]){
    Object.assign(data,{price:this.prix_sac[0]})
  }

  if(this.sac_size==this.list_sac[1]){
    Object.assign(data,{price:this.prix_sac[1]})
  }
  if(this.sac_size==this.list_sac[2]){
    Object.assign(data,{price:this.prix_sac[2]})
  }
  if(this.sac_size==this.list_sac[3]){
    Object.assign(data,{price:this.prix_sac[3]})
  }
  if(this.sac_size==this.list_sac[4]){
    Object.assign(data,{price:this.prix_sac[4]})
  }

  if(this.sac_size==this.list_sac[5]){
    Object.assign(data,{price:this.prix_sac[5]})
  }

  if(this.sac_size==this.list_sac[6]){
    Object.assign(data,{price:this.prix_sac[6]})
  }


  this.http.Insertpackdata(data).subscribe(
    res=>{
      if(res.data!=undefined){
        this.insertId=res.data.insertId
       // this.InsertpackImage();
       this.saveimages()
        console.log(res)
      }
      else{
        console.log(res)
        //this.toggle()
        myalert.fire({
        title: "Erreur",
        text: res.massage,
        icon: "error",
         button: "Ok"
        })

      }
    },
err=>{
  console.log(err)
 // this.toggle()
  
}
  )


}


saveimages(){
  let data={
    url1:this.url3,
    url2:this.url3,
    url3:this.url3,
    url4:this.url3,
    insertId:this.insertId
  }
  this.http.InsertpackImage(data).subscribe(
    res=>{
      if(res.data!=undefined){
        this.insertId=res.data.insertId
       // this.InsertpackImage()
        console.log(res)
      }
      else{
        console.log(res)
        //this.toggle()
        myalert.fire({
        title: "Erreur",
        text: res.massage,
        icon: "error",
         button: "Ok"
        })

      }
    },
err=>{
  console.log(err)
 // this.toggle()
  
}
  )
}

gotoFourth(){
  this.third=!this.third;
  this.fourth=!this.fourth;
  if(!this.sac){
    this.sachet=!this.sachet;
    this.sh=!this.sh

  }
}

getbacktofirst(){
  this.fourth=!this.fourth;
  this.first=!this.first;
}
  changeComplete($event:ColorEvent){
    this.enteredcolor=$event.color.hex;
    this.hascolor=true;

  }



  uploadfile1(event:any){
    console.log(event)
    let file=event.target.files[0];
    const reader = new FileReader();
      reader.onload = () => {
       this.url1 = reader.result;
       };
      reader.readAsDataURL(file);

  }

  uploadfile2(event:any){
    let file=event.target.files[0];
    const reader = new FileReader();
      reader.onload = () => {
       this.url2 = reader.result;
       };
      reader.readAsDataURL(file);
    

  }

  uploadfile3(event:any){
    this.url3=event.target.files[0]
      let file:any
    const reader = new FileReader();
      reader.onload = () => {
       file = reader.result;
       };
      reader.readAsDataURL(this.url3);

  }

  uploadfile4(event:any){
    let file=event.target.files[0];
    const reader = new FileReader();
      reader.onload = () => {
       this.url4 = reader.result;
       };
      reader.readAsDataURL(file);

  }


  uploadfile5(event:any){
    let file=event.target.files[0];
    const reader = new FileReader();
      reader.onload = () => {
       this.url5 = reader.result;
       };
      reader.readAsDataURL(file);

  }

  uploadfile6(event:any){
    let file=event.target.files[0];
    const reader = new FileReader();
      reader.onload = () => {
       this.url6 = reader.result;
       };
      reader.readAsDataURL(file);

  }




}
