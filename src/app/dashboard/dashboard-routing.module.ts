import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import{AdminGuard} from '../services/admin.guard';
const routes:Routes=[
  {
    path:'dashboard',component:DashboardComponent,
  // canActivate:[AdminGuard]
}


]

@NgModule({
  declarations: [],

  imports: [RouterModule.forChild(routes),CommonModule
  ],

})
export class DashboardRoutingModule { }
