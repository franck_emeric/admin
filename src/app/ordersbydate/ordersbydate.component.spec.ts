import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersbydateComponent } from './ordersbydate.component';

describe('OrdersbydateComponent', () => {
  let component: OrdersbydateComponent;
  let fixture: ComponentFixture<OrdersbydateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersbydateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersbydateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
