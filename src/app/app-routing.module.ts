import { NgModule } from '@angular/core';
import { RouterModule, Routes,PreloadAllModules } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: '/login',
    pathMatch: 'full',
},
  {path:'admin', loadChildren:()=>import('./dashboard/dashboard.module').then(m=>m.DashboardModule)},
  {path:'designers', loadChildren:()=>import('./designer/designer.module').then(m=>m.DesignerModule)},
 
  {path:'customer', loadChildren:()=>import('./conseilclients/conseilclients.module').then(c=>c.ConseilclientsModule)},

 {path:'production', loadChildren:()=>import('./productions/productions.module').then(p=>p.ProductionsModule)},

 {path: 'login', component: LoginComponent},
 {path: 'menu', component: MenuComponent}

]
@NgModule({
  imports: [RouterModule.forRoot(routes,{
    preloadingStrategy:PreloadAllModules,
    enableTracing:true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
